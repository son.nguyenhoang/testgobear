package sonnguyenh.com.gobeartest.login;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseActivity;
import sonnguyenh.com.gobeartest.databinding.ActivityLoginBinding;
import sonnguyenh.com.gobeartest.schema.request.LoginRequest;
import sonnguyenh.com.gobeartest.util.PrefUtils;

import java.util.Objects;

import static sonnguyenh.com.gobeartest.util.Constants.PREF_USER;

public class LoginActivity extends BaseActivity implements LoginActionListener {
    private LoginRepo repo = new LoginRepo();
    private LoginViewModel viewModel;
    private SweetAlertDialog progressHUD;
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setListener(this);
        setupView();
        observeStream();
    }

    @Override
    protected void setupView() {
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()).create(LoginViewModel.class);
        viewModel.setLoginRepo(repo);
        progressHUD = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressHUD.getProgressHelper().setBarColor(getColor(R.color.progress_color));
        progressHUD.setTitleText(getString(R.string.lb_loading));
        progressHUD.setCancelable(false);
    }

    @Override
    protected void observeStream() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        repo.onDestroy();
        repo = null;
        progressHUD = null;
    }

    @Override
    public void onLoginClicked() {
        if (!viewModel.isValidData(binding.edUserName.getText(), binding.edPass.getText())) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.lb_error))
                    .setContentText(getString(R.string.lb_login_wrong_input))
                    .show();
            return;
        }
        LoginRequest request = new LoginRequest(Objects.requireNonNull(binding.edUserName.getText()).toString(), Objects.requireNonNull(binding.edPass.getText()).toString());
        viewModel.login(request).observe(this, loginResponse -> {
            if (loginResponse == null) {
                return;
            }
            if (loginResponse.getStatus() != null) {
                switch (loginResponse.getStatus()) {
                    case DONE:
                        if (progressHUD != null) {
                            progressHUD.dismiss();
                        }
                        break;
                    case LOADING:
                        if (progressHUD != null) {
                            progressHUD.show();
                        }
                        return;
                }
            }
            if (loginResponse.isSuccess()) {
                if (binding.swRememberMe.isChecked()) {
                    PrefUtils.saveToPrefs(this, PREF_USER, loginResponse.getUser().toString());
                }
                setResult(RESULT_OK);
                finish();
            } else {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setContentText(loginResponse.getMessage())
                        .show();
            }
        });
    }
}
