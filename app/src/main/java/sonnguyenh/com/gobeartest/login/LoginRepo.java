package sonnguyenh.com.gobeartest.login;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import org.jetbrains.annotations.NotNull;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.schema.LoginResponse;
import sonnguyenh.com.gobeartest.schema.User;
import sonnguyenh.com.gobeartest.schema.request.LoginRequest;
import sonnguyenh.com.gobeartest.util.Constants;

import java.util.UUID;

import static sonnguyenh.com.gobeartest.util.Utils.getString;

public class LoginRepo {
    private Disposable loginDispose;

    MutableLiveData<LoginResponse> login(LoginRequest request) {
        MutableLiveData<LoginResponse> response = new MutableLiveData<>();
        if (request == null) {
            return response;
        }
        loginDispose = checkLogin(request)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response::setValue);
        return response;
    }

    private Observable<LoginResponse> checkLogin(@NotNull LoginRequest request) {
        LoginResponse loginResponse = new LoginResponse();
        if (Constants.DummyLogin.userName.endsWith(request.getUserName()) && Constants.DummyLogin.pass.equals(request.getPass())) {
            User user = new User(Constants.DummyLogin.userName, UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());
            loginResponse.setUser(user);
            loginResponse.setMessage(getSuccessMessage());
            loginResponse.setSuccess(true);
        } else {
            loginResponse.setMessage(getFailedMessage());
            loginResponse.setSuccess(false);
        }
        return Observable.just(loginResponse);
    }

    protected String getSuccessMessage() {
        return getString(R.string.lb_login_success);
    }

    protected String getFailedMessage() {
        return getString(R.string.lb_login_failed);
    }

    void onDestroy() {
        if (loginDispose != null) {
            loginDispose.dispose();
        }
    }
}
