package sonnguyenh.com.gobeartest.login;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import sonnguyenh.com.gobeartest.schema.LoginResponse;
import sonnguyenh.com.gobeartest.schema.request.LoginRequest;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginViewModel extends ViewModel {
    private LoginRepo loginRepo;

    public LoginViewModel() {
    }

    public void setLoginRepo(LoginRepo loginRepo) {
        this.loginRepo = loginRepo;
    }

    public MutableLiveData<LoginResponse> login(LoginRequest request) {
        checkNotNull(loginRepo);
        return loginRepo.login(request);
    }

    public boolean isValidData(CharSequence userName, CharSequence pass) {
        return (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pass));
    }
}
