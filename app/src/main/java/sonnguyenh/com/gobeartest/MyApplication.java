package sonnguyenh.com.gobeartest;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

    private static Application context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = this;
    }

    public static Application getAppContext() {
        return context;
    }
}