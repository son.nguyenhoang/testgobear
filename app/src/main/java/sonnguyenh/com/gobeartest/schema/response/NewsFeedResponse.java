package sonnguyenh.com.gobeartest.schema.response;

import sonnguyenh.com.gobeartest.schema.FetchStatus;
import sonnguyenh.com.gobeartest.schema.NewsFeedItem;

import java.util.List;

public class NewsFeedResponse {
    private FetchStatus fetchStatus = FetchStatus.DONE;
    private List<NewsFeedItem> newsFeedItems;

    public FetchStatus getFetchStatus() {
        return fetchStatus;
    }

    public void setFetchStatus(FetchStatus fetchStatus) {
        this.fetchStatus = fetchStatus;
    }

    public List<NewsFeedItem> getNewsFeedItems() {
        return newsFeedItems;
    }

    public void setNewsFeedItems(List<NewsFeedItem> newsFeedItems) {
        this.newsFeedItems = newsFeedItems;
    }
}
