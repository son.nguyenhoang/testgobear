package sonnguyenh.com.gobeartest.schema.request;

public class LoginRequest {
    private String userName;
    private String pass;

    public LoginRequest(String userName, String pass) {
        this.userName = userName;
        this.pass = pass;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
