package sonnguyenh.com.gobeartest.schema;

import com.google.gson.Gson;

public class User {
    private String userName;
    private String token;

    public User(String userName, String token) {
        this.userName = userName;
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String toString() {
        try {
            return new Gson().toJson(this);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
