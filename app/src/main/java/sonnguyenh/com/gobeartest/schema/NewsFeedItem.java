package sonnguyenh.com.gobeartest.schema;

import com.prof.rssparser.Article;

public class NewsFeedItem {
    private Article article;

    public NewsFeedItem(Article article) {
        this.article = article;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
