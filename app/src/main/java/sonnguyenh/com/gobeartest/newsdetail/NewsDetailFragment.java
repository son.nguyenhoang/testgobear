package sonnguyenh.com.gobeartest.newsdetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseFragment;
import sonnguyenh.com.gobeartest.databinding.FragmentNewsDetailWebViewBinding;

public class NewsDetailFragment extends BaseFragment {
    private FragmentNewsDetailWebViewBinding binding;
    private DetailViewModel viewModel;
    private WebViewClient client;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_news_detail_web_view, container, false);
        binding = FragmentNewsDetailWebViewBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView();
        observeStream();
    }

    @Override
    protected void setupView() {
        if (getActivity() == null) {
            return;
        }
        viewModel = ViewModelProviders.of(getActivity()).get(DetailViewModel.class);
        binding.lnToolBar.setOnClickListener(view -> {
            if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });
        binding.webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress < 100 && binding.progressBar.getVisibility() == View.GONE) {
                    binding.progressBar.setVisibility(View.VISIBLE);
                }

                binding.progressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    binding.progressBar.setVisibility(View.GONE);
                    disableWebViewClick();
                }
            }
        });
        client = new WebViewClient();
        binding.webView.setWebViewClient(client);
    }

    private void disableWebViewClick() {
        if (!isAdded()) {
            return;
        }
        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return true;
            }
        });
    }

    @Override
    protected void observeStream() {
        viewModel.getArticleMutableLiveData().observe(this, article -> {
            if (article == null || !isAdded()) {
                return;
            }
            binding.tvTitle.setText(article.getTitle());
            if (article.getLink() != null) {
                binding.webView.loadUrl(article.getLink());
            }
        });
    }
}
