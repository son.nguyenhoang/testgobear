package sonnguyenh.com.gobeartest.newsdetail;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.prof.rssparser.Article;

public class DetailViewModel extends AndroidViewModel {
    private MutableLiveData<Article> articleMutableLiveData = new MutableLiveData<>();

    public DetailViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Article> getArticleMutableLiveData() {
        return articleMutableLiveData;
    }
}
