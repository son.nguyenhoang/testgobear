package sonnguyenh.com.gobeartest.newsdetail;

import android.os.Bundle;
import androidx.databinding.DataBindingUtil;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseActivity;

public class NewsDetailActivity extends BaseActivity {
    public static final String ARG_DATA = "ARG_DATA";

    @Override
    protected void setupView() {

    }

    @Override
    protected void observeStream() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
