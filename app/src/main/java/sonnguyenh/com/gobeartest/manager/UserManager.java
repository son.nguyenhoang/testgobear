package sonnguyenh.com.gobeartest.manager;

import android.text.TextUtils;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import sonnguyenh.com.gobeartest.MyApplication;
import sonnguyenh.com.gobeartest.schema.User;
import sonnguyenh.com.gobeartest.util.Constants;
import sonnguyenh.com.gobeartest.util.PrefUtils;

public class UserManager {
    private static final UserManager ourInstance = new UserManager();

    public static UserManager getInstance() {
        return ourInstance;
    }

    private User user;

    private UserManager() {
    }

    public User getUser() {
        if (user == null) {
            user = loadFromPref();
        }
        return user;
    }

    private User loadFromPref() {
        User user = null;
        String json = PrefUtils.getFromPrefs(MyApplication.getAppContext(), Constants.PREF_USER, "");
        if (!TextUtils.isEmpty(json)) {
            try {
                user = new Gson().fromJson(json, User.class);

            } catch (JsonSyntaxException e) {
                Log.e(UserManager.class.getCanonicalName(), e.toString());
            }
        }
        return user;
    }

    public void clear() {
        user = null;
        PrefUtils.saveToPrefs(MyApplication.getAppContext(), Constants.PREF_USER, "");
    }
}
