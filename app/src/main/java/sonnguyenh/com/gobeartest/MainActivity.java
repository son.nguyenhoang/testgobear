package sonnguyenh.com.gobeartest;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import sonnguyenh.com.gobeartest.base.BaseActivity;
import sonnguyenh.com.gobeartest.introduce.WalkThroughActionListener;
import sonnguyenh.com.gobeartest.introduce.WalkThroughParentFragment;
import sonnguyenh.com.gobeartest.login.LoginActivity;
import sonnguyenh.com.gobeartest.manager.UserManager;
import sonnguyenh.com.gobeartest.newsdetail.NewsDetailFragment;
import sonnguyenh.com.gobeartest.newsfeed.NewsFeedActionListener;
import sonnguyenh.com.gobeartest.newsfeed.NewsFeedCallBack;
import sonnguyenh.com.gobeartest.newsfeed.NewsFeedFragment;
import sonnguyenh.com.gobeartest.schema.NewsFeedItem;
import sonnguyenh.com.gobeartest.util.ActivityUtils;
import sonnguyenh.com.gobeartest.util.Constants;
import sonnguyenh.com.gobeartest.util.PrefUtils;

import static sonnguyenh.com.gobeartest.util.Constants.SHOW_WALKTHROUGH;

public class MainActivity extends BaseActivity implements WalkThroughActionListener, NewsFeedCallBack, NewsFeedActionListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkShowWalkThrough();
    }

    private void checkShowWalkThrough() {
        if (PrefUtils.getFromPrefs(this, SHOW_WALKTHROUGH, true)) {
            ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), WalkThroughParentFragment.newInstance(), R.id.contentFrame);
        } else if (UserManager.getInstance().getUser() == null) {
            showLoginPage();
        } else {
            showListPage();
        }
    }

    private void showListPage() {
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), new NewsFeedFragment(), R.id.contentFrame);
    }

    private void showLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, Constants.RequestCode.LOGIN);
    }


    @Override
    protected void setupView() {

    }

    @Override
    protected void observeStream() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.RequestCode
                    .LOGIN:
                if (resultCode == RESULT_OK) {
                    showFeed();
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }

    private void showFeed() {
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), new NewsFeedFragment(), R.id.contentFrame);
    }

    @Override
    public void onSkipWalkThrough() {
        showLoginPage();
    }

    @Override
    public void onNewsFeedItemSelected(NewsFeedItem item) {
        ActivityUtils.addFragment(getSupportFragmentManager(), new NewsDetailFragment(), "NewsFeedDetail", R.id.contentFrame);
    }

    @Override
    public void onLogoutClicked() {
        UserManager.getInstance().clear();
        showLoginPage();
    }
}
