package sonnguyenh.com.gobeartest.newsfeed;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.prof.rssparser.Article;
import com.squareup.picasso.Picasso;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.databinding.ItemNewsFeedBinding;
import sonnguyenh.com.gobeartest.schema.NewsFeedItem;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder> {
    private final List<NewsFeedItem> dataSource = new ArrayList<>();
    private NewsFeedCallBack callBack;

    public NewsFeedAdapter(NewsFeedCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public NewsFeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_feed, parent, false);
        return new NewsFeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsFeedViewHolder holder, int position) {
        holder.onBind(dataSource.get(position), callBack);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public void setDataSource(List<NewsFeedItem> list) {
        dataSource.clear();
        dataSource.addAll(list);
        notifyDataSetChanged();
    }

    public static class NewsFeedViewHolder extends RecyclerView.ViewHolder {
        private ItemNewsFeedBinding binding;

        public NewsFeedViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemNewsFeedBinding.bind(itemView);
        }

        public void onBind(NewsFeedItem item, NewsFeedCallBack callBack) {
            clearHolder();
            if (item == null || item.getArticle() == null) {
                return;
            }
            Article article = item.getArticle();
            Picasso.get().load(article.getImage()).resize(80, 80).into(binding.imgView);
            binding.tvContent.setText(article.getDescription());
            binding.tvTitle.setText(article.getTitle());
            binding.tvDate.setText(article.getPubDate());
            itemView.setOnClickListener(view -> {
                if (callBack != null) {
                    callBack.onNewsFeedItemSelected(item);
                }
            });
        }

        private void clearHolder() {
            binding.imgView.setBackground(null);
            binding.tvContent.setText("");
            binding.tvDate.setText("");
            binding.tvContent.setText("");
        }
    }
}
