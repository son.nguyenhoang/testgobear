package sonnguyenh.com.gobeartest.newsfeed;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseFragment;
import sonnguyenh.com.gobeartest.databinding.FragmentBannerBinding;
import sonnguyenh.com.gobeartest.databinding.FragmentNewsFeedBinding;

public class BannerFragment extends BaseFragment {
    private static final String ARG_URL = "ARG_URL";
    private static final String ARG_POS = "ARG_POS";
    private FragmentBannerBinding binding;
    private String url = "";
    private int position = 0;

    public static BannerFragment newInstance(String url, int position) {
        BannerFragment fragment = new BannerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        bundle.putInt(ARG_POS, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().getString(ARG_URL) != null) {
            url = getArguments().getString(ARG_URL);
            position = getArguments().getInt(ARG_POS);
        }
    }

    @Override
    protected void setupView() {
        if (url != null) {
            Picasso.get().load(url).into(binding.imgBanner);
        }
        binding.getRoot().setOnClickListener(view -> {
            if (getParentFragment() instanceof NewsFeedFragment) {
                ((NewsFeedFragment) getParentFragment()).onBannerClick(position);
            }
        });
    }

    @Override
    protected void observeStream() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_banner, container, false);
        binding = FragmentBannerBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        observeStream();
    }
}
