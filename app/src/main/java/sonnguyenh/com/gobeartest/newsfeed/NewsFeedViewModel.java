package sonnguyenh.com.gobeartest.newsfeed;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import sonnguyenh.com.gobeartest.schema.response.NewsFeedResponse;

public class NewsFeedViewModel extends AndroidViewModel {
    private NewsFeedRepo repo;
    private MutableLiveData<NewsFeedResponse> newFeedLiveData;
    public NewsFeedViewModel(@NonNull Application application) {
        super(application);
    }

    public void setRepo(NewsFeedRepo repo) {
        this.repo = repo;
    }

    public MutableLiveData<NewsFeedResponse> fetchNews() {
        return repo.getNews();
    }
}
