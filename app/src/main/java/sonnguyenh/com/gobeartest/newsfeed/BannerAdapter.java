package sonnguyenh.com.gobeartest.newsfeed;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.prof.rssparser.Article;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends FragmentStatePagerAdapter {
    private List<Article> list = new ArrayList<>();

    public List<Article> getList() {
        return list;
    }

    public void setList(List<Article> list) {
        this.list = list;
    }

    public BannerAdapter(FragmentManager fm, List<Article> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return BannerFragment.newInstance(list.get(position).getImage(), position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
