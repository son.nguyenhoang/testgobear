package sonnguyenh.com.gobeartest.newsfeed;


import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.prof.rssparser.Article;
import sonnguyenh.com.gobeartest.MyApplication;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseFragment;
import sonnguyenh.com.gobeartest.databinding.FragmentNewsFeedBinding;
import sonnguyenh.com.gobeartest.newsdetail.DetailViewModel;
import sonnguyenh.com.gobeartest.schema.NewsFeedItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFeedFragment extends BaseFragment implements NewsFeedCallBack, NewsFeedActionListener {

    private NewsFeedViewModel viewModel;
    private FragmentNewsFeedBinding binding;
    private NewsFeedAdapter adapter;
    private DetailViewModel detailViewModel;
    private BannerAdapter bannerAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_news_feed, container, false);
        binding = FragmentNewsFeedBinding.inflate(inflater, container, false);
        binding.setListener(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initProgressDialog();
        setupView();
        observeStream();
        fetchNews(true);
    }

    @Override
    protected void setupView() {
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(MyApplication.getAppContext()).create(NewsFeedViewModel.class);
        if (getActivity() != null) {
            detailViewModel = ViewModelProviders.of(getActivity()).get(DetailViewModel.class);
        }
        NewsFeedRepo repo = new NewsFeedRepo();
        viewModel.setRepo(repo);
        adapter = new NewsFeedAdapter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.top = 20;
                outRect.left = 15;
                outRect.right = 15;
            }
        });
        binding.swipeToRefresh.setOnRefreshListener(() -> fetchNews(false));
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (bannerAdapter == null || position < bannerAdapter.getCount()) {
                    return;
                }
                Article article = bannerAdapter.getList().get(position);
                if (article == null) {
                    return;
                }
                binding.tvBannerTitle.setText(article.getTitle());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void observeStream() {

    }

    private void setupBanner(List<NewsFeedItem> list) {
        if (list == null) {
            return;
        }
        List<Article> randomList = new ArrayList<>();
        int i = 0;
        while (i < 5) {
            randomList.add(list.get(new Random().nextInt(list.size() - 1)).getArticle());
            i++;
        }
        if (bannerAdapter == null) {
            bannerAdapter = new BannerAdapter(getChildFragmentManager(), randomList);
        } else {
            bannerAdapter.setList(randomList);
        }
        binding.viewPager.setAdapter(bannerAdapter);
        binding.viewPager.setCurrentItem(1);
        binding.tvBannerTitle.setVisibility(View.VISIBLE);
        binding.indicator.setVisibility(View.VISIBLE);
        binding.indicator.setViewPager(binding.viewPager);
    }

    private void fetchNews(boolean showLoading) {
        if (showLoading) {
            showLoading();
        }
        viewModel.fetchNews().observe(this, newsFeedResponse -> {
            if (newsFeedResponse == null || !isAdded()) {
                return;
            }
            switch (newsFeedResponse.getFetchStatus()) {
                case LOADING:
                    showLoading();
                    return;
                case DONE:
                    binding.swipeToRefresh.setRefreshing(false);
                    dismissLoading();
                    break;
            }
            if (newsFeedResponse.getNewsFeedItems() == null || newsFeedResponse.getNewsFeedItems().isEmpty()) {
                binding.tvNoResult.setVisibility(View.VISIBLE);
                return;
            }
            adapter.setDataSource(newsFeedResponse.getNewsFeedItems());
            setupBanner(newsFeedResponse.getNewsFeedItems());
        });
    }

    @Override
    public void onNewsFeedItemSelected(NewsFeedItem item) {
        if (detailViewModel != null && item != null && item.getArticle() != null) {
            detailViewModel.getArticleMutableLiveData().setValue(item.getArticle());
            if (getActivity() instanceof NewsFeedCallBack) {
                ((NewsFeedCallBack) getActivity()).onNewsFeedItemSelected(item);
            }
        }
    }

    void onBannerClick(int position) {
        if (bannerAdapter == null) {
            return;
        }
        Article article = bannerAdapter.getList().get(position);
        if (article == null) {
            return;
        }
        onNewsFeedItemSelected(new NewsFeedItem(article));
    }

    @Override
    public void onLogoutClicked() {
        if (getActivity() != null && getActivity() instanceof NewsFeedActionListener) {
            ((NewsFeedActionListener) getActivity()).onLogoutClicked();
        }
    }
}
