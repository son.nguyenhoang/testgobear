package sonnguyenh.com.gobeartest.newsfeed;

import sonnguyenh.com.gobeartest.schema.NewsFeedItem;

public interface NewsFeedCallBack {
    void onNewsFeedItemSelected(NewsFeedItem item);
}
