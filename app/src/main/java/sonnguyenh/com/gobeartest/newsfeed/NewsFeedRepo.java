package sonnguyenh.com.gobeartest.newsfeed;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.prof.rssparser.Article;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import org.jetbrains.annotations.NotNull;
import sonnguyenh.com.gobeartest.schema.FetchStatus;
import sonnguyenh.com.gobeartest.schema.NewsFeedItem;
import sonnguyenh.com.gobeartest.schema.response.NewsFeedResponse;
import sonnguyenh.com.gobeartest.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedRepo {
    private Disposable getFeed;

    public MutableLiveData<NewsFeedResponse> getNews() {
        MutableLiveData<NewsFeedResponse> liveData = new MutableLiveData<>();
        Parser parser = new Parser();
        NewsFeedResponse response = new NewsFeedResponse();
        response.setFetchStatus(FetchStatus.LOADING);
        parser.onFinish(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(@NotNull List<Article> list) {
                List<NewsFeedItem> items = new ArrayList<>();
                for (Article art : list) {
                    items.add(new NewsFeedItem(art));
                }
                response.setFetchStatus(FetchStatus.DONE);
                response.setNewsFeedItems(items);
                liveData.postValue(response);
            }

            @Override
            public void onError(@NotNull Exception e) {
                response.setFetchStatus(FetchStatus.DONE);
                liveData.postValue(response);
            }
        });
        parser.execute(Constants.FEED_URL);
        return liveData;
    }
}
