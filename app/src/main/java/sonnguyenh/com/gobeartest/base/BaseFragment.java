package sonnguyenh.com.gobeartest.base;

import androidx.fragment.app.Fragment;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sonnguyenh.com.gobeartest.R;

public abstract class BaseFragment extends Fragment {
    protected abstract void setupView();

    protected abstract void observeStream();

    protected SweetAlertDialog progressHUD;

    protected void initProgressDialog() {
        if (getActivity() != null) {
            progressHUD = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            progressHUD.getProgressHelper().setBarColor(getActivity().getColor(R.color.progress_color));
            progressHUD.setTitleText(getString(R.string.lb_loading));
            progressHUD.setCancelable(false);
        }
    }

    protected void showLoading() {
        if (progressHUD != null) {
            progressHUD.show();
        }
    }

    protected void dismissLoading() {
        if (progressHUD != null) {
            progressHUD.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressHUD = null;
    }
}
