package sonnguyenh.com.gobeartest.introduce;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class WalkThroughPagerAdapter extends FragmentStatePagerAdapter {
    private int pageSize;
    WalkThroughPagerAdapter(FragmentManager fm, int pageSize) {
        super(fm);
        this.pageSize = pageSize;
    }

    @Override
    public Fragment getItem(int position) {
        return WalkThroughFragment.newInstance(String.valueOf(position));
    }

    @Override
    public int getCount() {
        return pageSize;
    }

}
