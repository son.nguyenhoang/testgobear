package sonnguyenh.com.gobeartest.introduce;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import org.jetbrains.annotations.NotNull;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseFragment;
import sonnguyenh.com.gobeartest.databinding.FragmentWalkthroughParentBinding;
import sonnguyenh.com.gobeartest.schema.WalkThrough;
import sonnguyenh.com.gobeartest.util.PrefUtils;

import static sonnguyenh.com.gobeartest.util.Constants.SHOW_WALKTHROUGH;

public class WalkThroughParentFragment extends BaseFragment implements WalkThroughActionListener {

    private WalkThoughViewModel viewModel;
    private WalkThroughRepo repo;
    private FragmentWalkthroughParentBinding binding;

    public WalkThroughParentFragment() {
        // Required empty public constructor
    }

    public static WalkThroughParentFragment newInstance() {
        WalkThroughParentFragment fragment = new WalkThroughParentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_walkthrough_parent, container, false);
        binding = FragmentWalkthroughParentBinding.inflate(inflater, container, false);
        binding.setListener(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView();
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(WalkThoughViewModel.class);
            viewModel.setRepo(repo);
            viewModel.initData(getContext());
        }
        observeStream();
    }

    @Override
    protected void setupView() {
        if (repo == null) {
            repo = new WalkThroughRepo();
        }

        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (viewModel == null || repo == null) {
                    return;
                }
                viewModel.setShowNextBtn(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupViewPager() {
        WalkThroughPagerAdapter adapter = new WalkThroughPagerAdapter(getChildFragmentManager(), repo.getTotalPage());
        binding.viewpager.setAdapter(adapter);
        binding.indicator.setViewPager(binding.viewpager);
    }

    @Override
    protected void observeStream() {
        if (viewModel == null) {
            return;
        }
        viewModel.isShowNextBtn().observe(this, visible -> binding.tvSkip.setText(visible ? getString(R.string.tv_next) : getString(R.string.tv_skip)));
        viewModel.getContents().observe(this, walkThroughs -> {
            if (walkThroughs == null) {
                return;
            }
            setupViewPager();
            int i = binding.viewpager.getCurrentItem();
            if (i < walkThroughs.size()) {
                WalkThrough walkThrough = walkThroughs.get(i);
                binding.tvGoBear.setText(walkThrough.getTitle());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    @Override
    public void onSkipWalkThrough() {
        PrefUtils.saveToPrefs(getContext(), SHOW_WALKTHROUGH, false);
        if (getActivity() != null && getActivity() instanceof WalkThroughActionListener) {
            ((WalkThroughActionListener) getActivity()).onSkipWalkThrough();
        }
    }
}

