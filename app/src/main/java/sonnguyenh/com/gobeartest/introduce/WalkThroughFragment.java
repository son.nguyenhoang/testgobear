package sonnguyenh.com.gobeartest.introduce;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import org.jetbrains.annotations.NotNull;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.base.BaseFragment;
import sonnguyenh.com.gobeartest.databinding.FragmentWalkthroughBinding;
import sonnguyenh.com.gobeartest.schema.WalkThrough;

public class WalkThroughFragment extends BaseFragment {

    private static final String ARG_TAG = "param1";
    private FragmentWalkthroughBinding binding;
    private String tag;
    private WalkThoughViewModel viewModel;

    public WalkThroughFragment() {
    }

    static WalkThroughFragment newInstance(String tag) {
        WalkThroughFragment fragment = new WalkThroughFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TAG, tag);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString(ARG_TAG);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_walkthrough, container, false);
        binding = FragmentWalkthroughBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(WalkThoughViewModel.class);
            viewModel.initData(getContext());
        }
        setupView();
        observeStream();
    }

    @Override
    protected void setupView() {

    }

    @Override
    protected void observeStream() {
        if (viewModel == null) {
            return;
        }
        viewModel.getContents().observe(this, walkThroughs -> {
            if (walkThroughs == null) {
                return;
            }
            int i = 0;
            try {
                i = Integer.valueOf(tag);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (i < walkThroughs.size()) {
                WalkThrough walkThrough = walkThroughs.get(i);
                binding.tvBody.setText(walkThrough.getContent());
            }
        });
    }
}
