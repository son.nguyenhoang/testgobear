package sonnguyenh.com.gobeartest.introduce;

import android.content.Context;
import androidx.lifecycle.MutableLiveData;
import sonnguyenh.com.gobeartest.R;
import sonnguyenh.com.gobeartest.schema.WalkThrough;

import java.util.ArrayList;
import java.util.List;

public class WalkThroughRepo {
    private List<WalkThrough> contents = new ArrayList<>();
    private static final int MAX_PAGE = 3;
    public WalkThroughRepo() {
    }

    MutableLiveData<List<WalkThrough>> getContents() {
        final MutableLiveData<List<WalkThrough>> data = new MutableLiveData<>();
        data.setValue(contents);
        return data;
    }

    public void setContents(List<WalkThrough> contents) {
        this.contents = contents;
    }

    void initDummyData(Context context) {
        if (context == null) {
            return;
        }
        contents.clear();
        WalkThrough a = new WalkThrough(context.getString(R.string.walkthough_title), context.getString(R.string.walkthough_a));
        int i = 0;
        while (i < MAX_PAGE) {
            contents.add(a);
            i++;
        }
    }

    public int getTotalPage() {
        return contents.size();
    }


}
