package sonnguyenh.com.gobeartest.introduce;

import android.content.Context;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import sonnguyenh.com.gobeartest.schema.WalkThrough;

import java.util.List;

public class WalkThoughViewModel extends ViewModel {
    private MutableLiveData<List<WalkThrough>> contents;
    private MutableLiveData<Boolean> showNextBtn = new MutableLiveData<>();
    private WalkThroughRepo repo;

    public WalkThoughViewModel() {
    }


    public void setRepo(WalkThroughRepo repo) {
        this.repo = repo;
        contents = repo.getContents();
    }

    MutableLiveData<List<WalkThrough>> getContents() {
        return contents;
    }

    public MutableLiveData<Boolean> isShowNextBtn() {
        if (showNextBtn == null) {
            showNextBtn = new MutableLiveData<>();
        }
        return showNextBtn;
    }

    void setShowNextBtn(int page) {
        if (repo == null) {
            return;
        }
        isShowNextBtn().setValue(showNext(page));
    }

    public boolean showNext(int page) {
        return page == repo.getTotalPage() - 1;
    }

    void initData(Context context) {
        if (repo == null) {
            return;
        }
        repo.initDummyData(context);
    }
}

