package sonnguyenh.com.gobeartest.util;

public class Constants {
    public static final String SHOW_WALKTHROUGH = "SHOW_WALKTHROUGH";
    public static final String PREF_USER = "PREF_USER";
    public static final String FEED_URL = "http://feeds.bbci.co.uk/news/world/asia/rss.xml";

    public static class DummyLogin {
        public static final String userName = "GoBear";
        public static final String pass = "GoBearDemo";
    }

    public static class RequestCode {
        public static final int LOGIN = 1000;
    }
}
