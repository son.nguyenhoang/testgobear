package sonnguyenh.com.gobeartest;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import sonnguyenh.com.gobeartest.login.LoginRepo;
import sonnguyenh.com.gobeartest.login.LoginViewModel;
import sonnguyenh.com.gobeartest.schema.request.LoginRequest;

@RunWith(RobolectricTestRunner.class)
public class LoginUnitTest extends BaseUnitTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    private LoginViewModel mLoginViewModel;

    @Before
    public void setUp() {
        initLifecycle();
        mLoginViewModel = new LoginViewModel();
        LoginRepo mLoginRepo = new LoginRepo() {
            @Override
            protected String getSuccessMessage() {
                return "Success";
            }

            @Override
            protected String getFailedMessage() {
                return "Failed";
            }
        };
        mLoginViewModel.setLoginRepo(mLoginRepo);
    }

    @Test
    public void validate_data_not_empty_isTrue() {
        Assert.assertFalse(mLoginViewModel.isValidData("", ""));
        Assert.assertFalse(mLoginViewModel.isValidData(null, null));
        Assert.assertFalse(mLoginViewModel.isValidData("dwadwa", null));
        Assert.assertFalse(mLoginViewModel.isValidData(null, "adwdawd"));
        Assert.assertTrue(mLoginViewModel.isValidData("userName", "pass"));
    }

    @Test
    public void validate_login_success() {
        mLoginViewModel.login(new LoginRequest("GoBear", "GoBearDemo")).observe(this, loginResponse -> {
            Assert.assertNotNull(loginResponse);
            Assert.assertTrue(loginResponse.isSuccess());
            Assert.assertNotNull(loginResponse.getUser());
        });
    }

    @Test
    public void validate_login_failed() {
        mLoginViewModel.login(new LoginRequest("GoBearr", "GoBearDemoe")).observe(this, loginResponse -> {
            Assert.assertNotNull(loginResponse);
            Assert.assertFalse(loginResponse.isSuccess());
            Assert.assertNull(loginResponse.getUser());
        });
    }
}
