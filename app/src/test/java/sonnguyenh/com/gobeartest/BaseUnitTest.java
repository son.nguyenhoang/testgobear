package sonnguyenh.com.gobeartest;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;

public class BaseUnitTest implements LifecycleOwner {
    private LifecycleRegistry mLifecycleRegistry;

    void initLifecycle() {
        if (mLifecycleRegistry == null || mLifecycleRegistry.getCurrentState() == Lifecycle.State.DESTROYED) {
            mLifecycleRegistry = new LifecycleRegistry(this);
            mLifecycleRegistry.markState(Lifecycle.State.CREATED);
            mLifecycleRegistry.markState(Lifecycle.State.STARTED);
        }
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }


}
