package sonnguyenh.com.gobeartest;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import sonnguyenh.com.gobeartest.introduce.WalkThoughViewModel;
import sonnguyenh.com.gobeartest.introduce.WalkThroughRepo;
import sonnguyenh.com.gobeartest.schema.WalkThrough;

import java.util.ArrayList;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class WalkThroughUnitTest extends BaseUnitTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    private WalkThroughRepo mRepo;
    private WalkThoughViewModel mWalkThoughViewModel;

    @Before
    public void setUp() {
        initLifecycle();
        mWalkThoughViewModel = new WalkThoughViewModel();
        mRepo = new WalkThroughRepo();
    }

    @Test
    public void showNextButton_returnTrue() {

        List<WalkThrough> list = getDummyWalkThrough();
        mRepo.setContents(list);
        mWalkThoughViewModel.setRepo(mRepo);
        mWalkThoughViewModel.isShowNextBtn().observe(this, Assert::assertTrue);
        mWalkThoughViewModel.showNext(mRepo.getTotalPage() - 1);
    }

    private List<WalkThrough> getDummyWalkThrough() {
        List<WalkThrough> list = new ArrayList<>();
        int i = 0;
        while (i < 3) {
            list.add(new WalkThrough("Title", "Content"));
            i++;
        }
        return list;
    }

    @Test
    public void showNextButton_returnFalse() {
        List<WalkThrough> list = getDummyWalkThrough();
        mRepo.setContents(list);
        mWalkThoughViewModel.setRepo(mRepo);
        mWalkThoughViewModel.isShowNextBtn().observe(this, Assert::assertTrue);
        mWalkThoughViewModel.showNext(0);
    }


}
